package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net"
	"net/http"
)

func main() {
	log.Fatal(Listen(":3000"))
}

func Listen(addr string) error {
	l, err := net.Listen("tcp", addr)
	if err != nil {
		return err
	}
	defer l.Close()

	for {
		conn, err := l.Accept()
		if err != nil {
			return err
		}
		go func(c net.Conn) {
			defer c.Close()
			// Server users should be able to  provide their own
			// implementation of the following code
			ip := c.RemoteAddr().(*net.TCPAddr).IP
			url := fmt.Sprintf("http://freegeoip.net/json/%s", ip)
			resp, err := http.Get(url)
			if err != nil {
				log.Println(err)
				c.Write([]byte("error fetching ip country\n"))
			}
			dec := json.NewDecoder(resp.Body)
			var ipMeta struct {
				CountryCode string `json:"country_code"`
			}
			if err := dec.Decode(&ipMeta); err != nil {
				c.Write([]byte("error fetching ip country\n"))
			}
			c.Write([]byte(ipMeta.CountryCode))

		}(conn)
	}

	return nil
}
