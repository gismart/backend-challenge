# Gismart Backend Challenge

`ip.go` is a simple TCP server that responds with client's country code.
It uses freegeoip.net API to fetch geoip data.
The goal of the challenge is to abstract out country detection code, so that
server users are able to provide their own implementation
(i.e using http://geoip.nekudo.com/ or local GeoIP database).
Consider moving everything to a separate package. Tests are welcome.

Send your code to HR manager as an archive by email or as a link to public git repo
